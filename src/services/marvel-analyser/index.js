const marvelWS = require('../marvel-ws')

const analyse = hero => {
  return marvelWS.getHeroId(hero).then(heroId => marvelWS.getSeries(heroId))
}

module.exports = {
  analyse
}
