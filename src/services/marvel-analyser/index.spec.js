const { expect, assert } = require('chai')
const { main } = require('./index')
const { stub } = require('sinon')
const marvelAnalyser = require('./index')
const marvelWS = require('../marvel-ws')

describe('MarvelAnalyser', () => {
  describe('analyse', () => {
    beforeEach(() => {
      stub(marvelWS, 'getHeroId').returns(Promise.resolve(42))
      stub(marvelWS, 'getSeries').returns(Promise.resolve(['serie1', 'serie2']))
    })

    afterEach(() => {
      marvelWS.getHeroId.restore()
      marvelWS.getSeries.restore()
    })

    it("should call marvelWS to get the hero's id", done => {
      marvelAnalyser.analyse('Spiderman').then(result => {
        assert(marvelWS.getHeroId.calledWith('Spiderman'))
        done()
      })
    })

    it("should call marvelWS to get the heroes's series", done => {
      marvelAnalyser.analyse('Spiderman').then(result => {
        assert(marvelWS.getSeries.calledWith(42))
        done()
      })
    })
  })
})
