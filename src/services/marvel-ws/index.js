const axios = require('axios')
const crypto = require('crypto')
const marvelKey = require('../../../marvel-token.json')

const computeMd5 = text =>
  crypto
    .createHash('md5')
    .update(text)
    .digest('hex')

const callMarvel = partialUrl => {
  const ts = new Date().getTime()
  const md5 = computeMd5(`${ts}${marvelKey.privateKey}${marvelKey.publicKey}`)
  const params = `ts=${ts}&apikey=${marvelKey.publicKey}&hash=${md5}`
  const partialUrlHasParams = partialUrl.indexOf('?') > 0
  const url = encodeURI(
    `http://gateway.marvel.com/v1/public/${partialUrl}${
      partialUrlHasParams ? '&' : '?'
    }${params}`
  )
  console.log(url)
  return axios.get(url)
}

const getHeroId = hero =>
  callMarvel(`characters?name=${hero}`).then(response => {
    console.log(response)
    return response.data.data.results[0].id
  })
const getSeries = heroId => callMarvel(`characters/${heroId}/series`)

module.exports = {
  getHeroId,
  getSeries
}
