const marvelAnalyser = require('./services/marvel-analyser')

const main = hero => {
  if (hero !== undefined) {
    console.log(`Analyzing our hero ${hero}`)
    return marvelAnalyser.analyse(hero)
  }
}
const mainHero = process.argv[2]
main(mainHero).then(infos => console.log(infos))

module.exports = {
  main
}
