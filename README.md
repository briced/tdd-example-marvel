This project is an example for demoing how to do TDD for a web request with map/filter/reduce after.

The differents git tags are milestone to key points.

Libs:
- https://github.com/axios/axios
- https://mochajs.org/
- http://chaijs.com/
- http://sinonjs.org
